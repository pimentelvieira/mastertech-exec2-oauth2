package com.mastertech.oauth2.repositories;

import com.mastertech.oauth2.models.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {
    List<Contato> findAllByIdUsuario(Integer idUsuario);
}
