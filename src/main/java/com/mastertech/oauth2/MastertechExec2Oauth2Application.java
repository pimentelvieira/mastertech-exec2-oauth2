package com.mastertech.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechExec2Oauth2Application {

	public static void main(String[] args) {
		SpringApplication.run(MastertechExec2Oauth2Application.class, args);
	}

}
