package com.mastertech.oauth2.controllers;

import com.mastertech.oauth2.models.Contato;
import com.mastertech.oauth2.security.Usuario;
import com.mastertech.oauth2.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ContatoController {

    @Autowired
    private ContatoService service;

    @PostMapping("/contato")
    public Contato criarContato(@RequestBody Contato contato, @AuthenticationPrincipal Usuario usuario) {
        contato.setIdUsuario(usuario.getId());
        return this.service.criar(contato);
    }

    @GetMapping("/contatos")
    public List<Contato> getAllByIdUsuario(@AuthenticationPrincipal Usuario usuario) {
        return this.service.getAllByIdUsuario(usuario.getId());
    }
}
